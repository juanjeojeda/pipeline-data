# Baseline testing from git trees

.default:
  cki_pipeline_project: cki-pipeline
  tree_name: upstream
  kernel_type: upstream
  builder_image: registry.gitlab.com/cki-project/containers/builder-fedora
  config_target: 'olddefconfig'
  make_target: 'targz-pkg'
  require_manual_review: 'False'
  report_types: 'email'
  report_template: limited
  mail_from: CKI Project <cki-project@redhat.com>
  mail_add_maintainers_to: 'cc'
  test_set: 'kt1'
  artifacts_visibility: public

.ark-base:
  git_url: https://gitlab.com/cki-project/kernel-ark.git
  .branches:
    - ark-latest
  cki_pipeline_branch: ark
  mail_to: kernel-team@fedoraproject.org
  arch_override: 'x86_64 ppc64le aarch64 s390x'
  send_report_on_success: 'True'
  make_target: rpm
  srpm_make_target: dist-srpm
  package_name: kernel
  build_kabi_whitelist: 'true'
  builder_image: registry.gitlab.com/cki-project/containers/builder-rawhide
  builder_image_tag: latest
  send_ready_for_test_pre: 'True'

ark-rawhide:
  .extends: .ark-base

ark-eln:
  .extends: .ark-base
  disttag_override: '.eln'
  fedora_macro_disable: 'true'

mainline.kernel.org:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
  .branches:
    - master
  cki_pipeline_branch: mainline.kernel.org
  arch_override: 'x86_64 ppc64le aarch64 s390x'
  report_template: full  # Remove this when enabling upstream emails !!!
  build_selftests: 'true'
  selftest_subsets: 'net bpf'

net-next:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/davem/net-next.git
  .branches:
    - master
  cki_pipeline_branch: net-next
  arch_override: 'x86_64 ppc64le aarch64 s390x'
  test_set: 'net'
  report_template: full  # Remove this when enabling upstream emails !!!
  build_selftests: 'true'
  selftest_subsets: net

rdma:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/rdma/rdma.git
  .branches:
    - for-next
    - for-rc
    - wip/jgg-for-rc
    - wip/jgg-for-next
    - wip/dl-for-rc
    - wip/dl-for-next
  cki_pipeline_branch: rdma
  mail_to: rdma-dev-team@redhat.com
  arch_override: 'x86_64'
  test_set: 'rdma'

scsi:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/mkp/scsi.git
  .branches:
    - for-next
  cki_pipeline_branch: scsi
  arch_override: 'aarch64 ppc64le x86_64 s390x'
  test_set: 'stor'
  report_template: full  # Remove this when enabling upstream emails !!!

arm-next:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/arm64/linux.git
  .branches:
    - for-kernelci
  cki_pipeline_branch: arm
  mail_to: 'will@kernel.org, catalin.marinas@arm.com, linux-arm-kernel@lists.infradead.org'
  arch_override: 'aarch64'

arm-acpi:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/lpieralisi/linux.git
  .branches:
    - acpi/for-next
  cki_pipeline_branch: arm
  mail_to: 'lorenzo.pieralisi@arm.com'
  arch_override: 'aarch64'
  test_set: 'acpi'

stable:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git
  .branches:
    - linux-5.9.y
  cki_pipeline_branch: upstream-stable
  mail_to: Linux Stable maillist <stable@vger.kernel.org>
  arch_override: 'aarch64 ppc64le x86_64 s390x'
  require_manual_review: 'True'

stable-queue:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git
  .branches:
    - queue/5.9
  cki_pipeline_branch: upstream-stable
  mail_to: Linux Stable maillist <stable@vger.kernel.org>
  arch_override: 'aarch64 ppc64le x86_64 s390x'
  require_manual_review: 'True'

stable-next:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/sashal/linux-stable.git
  .branches:
    - stable-next
  cki_pipeline_branch: upstream-stable
  mail_to: Linux Stable maillist <stable@vger.kernel.org>
  arch_override: 'aarch64 ppc64le x86_64 s390x'
  require_manual_review: 'True'

rt-devel:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-rt-devel.git
  .branches:
    - linux-5.9.y-rt
  cki_pipeline_branch: rt-devel
  mail_to: Kernel RT Team <kernel-rt-ci@redhat.com>
  arch_override: 'x86_64'
  rt_kernel: 'True'
  build_selftests: 'true'
  selftest_subsets: 'net livepatch bpf'

block-upt-test:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/axboe/linux-block.git
  .branches:
    - for-current
  cki_pipeline_branch: block
  arch_override: 'x86_64'
  test_set: 'kt0'
  send_report_on_success: 'False'
  test_mode: 'only_upt'
  test_runner: 'beaker'

block:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/axboe/linux-block.git
  .branches:
    - for-next
    - for-current
  cki_pipeline_branch: block
  mail_to: 'linux-block@vger.kernel.org, axboe@kernel.dk'
  success_override_to: 'axboe@kernel.dk'
  arch_override: 'aarch64 ppc64le x86_64 s390x'
  test_set: 'kt0|stor|fs'
  require_manual_review: 'True'
  send_report_on_success: 'False'

bpf-next:
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/bpf/bpf-next.git
  .branches:
    - master
  cki_pipeline_branch: bpf
  # keep the reports internal and DW-only until they are stable and without
  # known issues all the time
  # mail_to: 'new-upstream-list'
  report_template: full  # Remove this when enabling upstream emails !!!
  send_report_on_success: 'True'
  arch_override: 'x86_64'
  builder_image: registry.gitlab.com/cki-project/containers/builder-rawhide
  builder_image_tag: latest
  test_set: 'bpf'
  build_selftests: 'true'
  selftest_subsets: 'bpf'

kvm:
  git_url: https://git.kernel.org/pub/scm/virt/kvm/kvm.git
  .branches:
    - master
    - next
    - queue
  cki_pipeline_branch: kvm
  send_report_on_success: 'True'
  mail_to: 'mcondotta@redhat.com'
  arch_override: 'x86_64'
  test_set: 'virt'
  build_selftests: 'true'
  selftest_subsets: 'kvm'
